# Notification of Revoked Access Tokens in ACE

This is the working area for the individual Internet-Draft, "Notification of Revoked Access Tokens in the Authentication and Authorization for Constrained Environments (ACE) Framework".

* [Editor's Copy](https://crimson84.gitlab.io/draft-tiloca-ace-revoked-token-notification)
* [Individual Draft](https://datatracker.ietf.org/doc/html/draft-tiloca-ace-revoked-token-notification)
* [Compare Editor's Copy to Individual Draft](https://https:crimson84.github.io/gitlab.com/#go.draft-tiloca-ace-revoked-token-notification.diff)

## Building the Draft

Formatted text and HTML versions of the draft can be built using `make`.

```sh
$ make
```

This requires that you have the necessary software installed.  See
[the instructions](https://github.com/martinthomson/i-d-template/blob/main/doc/SETUP.md).


## Contributing

See the
[guidelines for contributions](https://github.com/martinthomson/i-d-template/blob/main/CONTRIBUTING.md).
